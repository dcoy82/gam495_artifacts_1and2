// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "UObject/ObjectMacros.h"
#include "Classes/Engine/TextureRenderTarget2D.h"
#include "Classes/Engine/World.h"
#include "Public/GlobalShader.h"
#include "Public/PipelineStateCache.h"
#include "Public/RHIStaticStates.h"
#include "Public/RHICommandList.h"
#include "Public/SceneUtils.h"
#include "Public/SceneInterface.h"
#include "Public/ShaderParameterUtils.h"
#include "RenderMyTest.generated.h"

/**
 * 
 */
UCLASS()
class COYSHADER_API URenderMyTest : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

		//UFUNCTION(BlueprintCallable, Category = "CoyShader | RenderMyShader")
		void RenderShader(FRHICommandList &RHICmdList, ERHIFeatureLevel::Type FeatureLevel, const FLinearColor& Color);

	 
};
