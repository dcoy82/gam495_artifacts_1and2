// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Runtime/CoreUObject//Public/UObject/ConstructorHelpers.h"
#include "GameFramework/Actor.h"
#include "Portal.generated.h"

UCLASS()
class PAINTBALL_COY_API APortal : public AActor
{
	GENERATED_BODY()
		
	
public:	
	// Sets default values for this actor's properties
	APortal();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** called when hit*/
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	//**Function to set portal to active
	void SetActive();

	bool CheckActive();

	//**coordinates for the transport location
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TransportCoords)
	FVector TransportCoords;

	//**rotation of the tranport location
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TransportCoords)
	FRotator TransportRotation;

	//**function to set the trandpost coords and rotation
	UFUNCTION()
	void SetTransCoords(FVector Coords, FRotator Rotation);

	//**function to get the transport coords
	UFUNCTION()
	FVector GetTransCoords();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	//**toggle for portal being activated
	bool isActive;

	//**have the transport coords been set
	bool coordsSet;

	//**elapsed time for deactivating portal
	float ElapsedTime;

	//**material interface 
	UMaterialInterface *PortalMat; 

	//**static mesh
	UStaticMeshComponent *CylinderMeshOne;

	//**reference to the game mode
	UPROPERTY()
	class APaintball_COYGameMode *GameMode;

	UPROPERTY()
	class APaintball_COYCharacter *PlayerChar;

	//**function to rotate portal towards the player
	void RotateToPlayer();
	
	
};
