// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/CoreUObject//Public/UObject/ConstructorHelpers.h"
#include "ExitPortal.generated.h"

UCLASS()
class PAINTBALL_COY_API AExitPortal : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExitPortal();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	//**material interface 
	UMaterialInterface *PortalMat;

	//**static mesh
	UStaticMeshComponent *CylinderMeshOne;

	//**elapsed time for deactivating portal
	float ElapsedTime;

	
	
};
