// Fill out your copyright notice in the Description page of Project Settings.

#include "FogActor.h"



// Sets default values
AFogActor::AFogActor():m_wholeTextureRegion(0, 0, 0, 0, m_textureSize, m_textureSize) //**initialize m_whileTextureRegion variable
{
 	//**set tick
	PrimaryActorTick.bCanEverTick = true;

	//**initialize cover size
	m_coverSize = 500; 

	//**create a square static mesh
	m_squarePlane = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fog Plane Static Mesh"));

	//**set to root component
	RootComponent = m_squarePlane;

	//**set collision profile
	m_squarePlane->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);

	//**use construction helper to get the plane mesh objesct
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("/Engine/ArtTools/RenderToTexture/Meshes/S_1_Unit_Plane.S_1_Unit_Plane"));
	//**set the plane's mesh to the asset just retrieved
	if (MeshAsset.Succeeded())
	{
		m_squarePlane->SetStaticMesh(MeshAsset.Object);
	}

	//**Set translucency sort priority
	m_squarePlane->TranslucencySortPriority = 100; 

	//**Set scale of mesh
	m_squarePlane->SetRelativeScale3D(FVector(m_coverSize, m_coverSize, 1));

	//**use constructor helper to get the fogmat object
	static ConstructorHelpers::FObjectFinder<UMaterial> MatAsset(TEXT("/Game/Shaders/FogMat.FogMat"));
	//**set the dynamic material to asset just retrieved
	if (MatAsset.Succeeded())
	{
		m_dynamicMaterial = MatAsset.Object;
	}

	
	//**create the runtime texture
	if (!m_dynamicTexture)
	{
		m_dynamicTexture = UTexture2D::CreateTransient(m_textureSize, m_textureSize, PF_G8); 
		m_dynamicTexture->CompressionSettings = TextureCompressionSettings::TC_Grayscale;
		m_dynamicTexture->SRGB = 0;
		//m_dynamicTexture->MipGenSettings = TMGS_NoMipmaps; 
		m_dynamicTexture->UpdateResource();
	}

	//**initialize the pixel array to all black
	for (int x = 0; x < m_textureSize; ++x)
	{
		for (int y = 0; y < m_textureSize; ++y)
		{
			m_pixelArray[y * m_textureSize + x] = 255; 
		}
	}

	//**update texture regions with new array data
	if (m_dynamicTexture)
	{
		UpdateTextureRegions(m_dynamicTexture, 0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray, false);
	}
	
}

// Called when the game starts or when spawned
void AFogActor::BeginPlay()
{
	Super::BeginPlay();

	myCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	
}

// Called every frame
void AFogActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (myCharacter != NULL)
	{
		//FVector2D playerPos = FVector2D(myCharacter->GetActorLocation().X, myCharacter->GetActorLocation().Y);
				
		//RevealSmoothCircle(playerPos, 200.0f);
	}
	
}

//**post initialization function
void AFogActor::PostInitializeComponents()
{
	//**call the cuper class function
	Super::PostInitializeComponents();

	/*///////////////////////////////////////////////////////////////////
	//**create the runtime texture
	if (!m_dynamicTexture)
	{
		m_dynamicTexture = UTexture2D::CreateTransient(m_textureSize, m_textureSize, PF_G8);
		m_dynamicTexture->CompressionSettings = TextureCompressionSettings::TC_Grayscale;
		m_dynamicTexture->SRGB = 0;
		m_dynamicTexture->MipGenSettings = TMGS_NoMipmaps;
		m_dynamicTexture->UpdateResource();
	}

	//**initialize the pixel array to all black
	for (int x = 0; x < m_textureSize; ++x)
	{
		for (int y = 0; y < m_textureSize; ++y)
		{
			m_pixelArray[y * m_textureSize + x] = 255;
		}
	}

	//**update texture regions with new array data
	if (m_dynamicTexture)
	{
		UpdateTextureRegions(m_dynamicTexture, 0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray, false);
	}
	/*////////////////////////////////////////////////////////////////////////
		
	//**if m_dynamicMaterial successfully initialize
	if (m_dynamicMaterial)
	{
		//**create dynamic material instance
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);
		//**set the dynamic texture
		m_dynamicMaterialInstance->SetTextureParameterValue("FogTexture", m_dynamicTexture);
	}

	//**if successfully instanced
	if (m_dynamicMaterialInstance)
	{
		//**set the dynamic material to the plane mesh
		m_squarePlane->SetMaterial(0, m_dynamicMaterialInstance);
	}
}

//**set size function
void AFogActor::SetSize(float Size)
{
	m_coverSize = Size;
	m_squarePlane->SetRelativeScale3D(FVector(m_coverSize, m_coverSize, 1));
}

//**reveal function
void AFogActor::RevealSmoothCircle(const FVector2D &pos, float radius)
{
	// Calculate the where circle center is inside texture space  
	FVector2D texel = pos - FVector2D(GetActorLocation().X, GetActorLocation().Y);
	texel = texel * m_textureSize / m_coverSize;
	texel += FVector2D(m_textureSize / 2, m_textureSize / 2); // Calculate radius in texel unit ( 1 is 1 pixel )  
	float texelRadius = radius * m_textureSize / m_coverSize; // The square area to update  
	int minX = FMath::Clamp <int>(texel.X - texelRadius, 0, m_textureSize - 1);
	int minY = FMath::Clamp <int>(texel.Y - texelRadius, 0, m_textureSize - 1);
	int maxX = FMath::Clamp <int>(texel.X + texelRadius, 0, m_textureSize - 1);
	int maxY = FMath::Clamp <int>(texel.Y + texelRadius, 0, m_textureSize - 1);
	
	uint8 theVal = 0; // Update our array of fog value in memory  
	bool dirty = false;
	for (int x = minX; x < maxX; ++x) {
		for (int y = minY; y < maxY; ++y) {
			float distance = FVector2D::Distance(texel, FVector2D(x, y));
			if (distance < texelRadius) {
				static float smoothPct = 0.7f;
				uint8 oldVal = m_pixelArray[y * m_textureSize + x];
				float lerp = FMath::GetMappedRangeValueClamped(FVector2D(smoothPct, 1.0f), FVector2D(0, 1), distance / texelRadius);
				uint8 newVal = lerp * 255;
				newVal = FMath::Min(newVal, oldVal);
				m_pixelArray[y * m_textureSize + x] = newVal;
				dirty = (dirty || oldVal != newVal);
			}
			
		}
	} 
	
	// Propagate the values in memory's array to texture.  
	if (dirty)
	{		
		UpdateTextureRegions(m_dynamicTexture, 0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray, false);
			
	}

}

void AFogActor::UpdateTextureRegions(UTexture2D * Texture, int32 MipIndex, uint32 NumRegions, FUpdateTextureRegion2D * Regions, uint32 SrcPitch, uint32 SrcBpp, uint8 * SrcData, bool bFreeData)
{
	
		//**if the Texture resource is loaded
		if (Texture->Resource)
		{
			struct FUpdateTextureRegionsData
			{
				FTexture2DResource * Texture2DResource;
				int32 MipIndex;
				uint32 NumRegions;
				FUpdateTextureRegion2D * Regions;
				uint32 SrcPitch;
				uint32 SrcBpp;
				uint8 * SrcData;
			};
						
			FUpdateTextureRegionsData * RegionData = new FUpdateTextureRegionsData;
			RegionData->Texture2DResource = (FTexture2DResource *)Texture->Resource;
			RegionData->MipIndex = MipIndex;
			RegionData->NumRegions = NumRegions;
			RegionData->Regions = Regions;
			RegionData->SrcPitch = SrcPitch;
			RegionData->SrcBpp = SrcBpp;
			RegionData->SrcData = SrcData;
					

			ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(UpdateTextureRegionsData, FUpdateTextureRegionsData *, RegionData, RegionData, bool, bFreeData, bFreeData, 
				{
					for (uint32 RegionIndex = 0; RegionIndex < RegionData->NumRegions; ++RegionIndex) 
					{
						int32 CurrentFirstMip = RegionData->Texture2DResource->GetCurrentFirstMip();
						if (RegionData->MipIndex >= CurrentFirstMip)
						{
							RHIUpdateTexture2D(RegionData->Texture2DResource->GetTexture2DRHI(), RegionData->MipIndex - CurrentFirstMip, RegionData->Regions[RegionIndex], RegionData->SrcPitch, RegionData->SrcData + RegionData->Regions[RegionIndex].SrcY * RegionData->SrcPitch + RegionData->Regions[RegionIndex].SrcX * RegionData->SrcBpp);
							
						}
					}
					if (bFreeData) 
					{
						FMemory::Free(RegionData->Regions);
						FMemory::Free(RegionData->SrcData);
					}
					delete RegionData;
				});

			

		}
	
}

