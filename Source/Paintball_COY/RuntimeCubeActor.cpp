// Fill out your copyright notice in the Description page of Project Settings.

//////////////////////////HEALTH PACK

#include "RuntimeCubeActor.h"


// Sets default values
ARuntimeCubeActor::ARuntimeCubeActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//**Initialie material color
	MatColor = FLinearColor(0.0, 0.0, 0.0);
	//**Initialize opacity for use to fade out object
	MatOpacity = 1.0f;
	//**initialize material glow
	MatGlow = 0.0f;

	//**initialize elapsed time
	ElapsedTime = 0.0f; 


	//**intialize the mesh component
	mesh = CreateDefaultSubobject<URuntimeMeshComponent>(TEXT("GeneratedMesh"));
	//**Set the OnHit Function
	mesh->OnComponentHit.AddDynamic(this, &ARuntimeCubeActor::OnHit);

	//**set the mesh as the root component
	RootComponent = mesh;

	//**use constructor helper to get the blue glow material object
	static ConstructorHelpers::FObjectFinder<UMaterial> MatAsset(TEXT("/Game/Materials/M_HealthPack.M_HealthPack"));
	//**set the dynamic material to asset just retrieved
	if (MatAsset.Succeeded())
	{
		m_dynamicMaterial = MatAsset.Object;
	}

}

// Called when the game starts or when spawned
void ARuntimeCubeActor::BeginPlay()
{
	Super::BeginPlay();

	//**Get game mode
	GameMode = Cast<APaintball_COYGameMode>(GetWorld()->GetAuthGameMode());

}

// Called every frame
void ARuntimeCubeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	//**add to elapsed time
	ElapsedTime += DeltaTime;

	//**if less than a second has elapsed
	if (ElapsedTime <= 1.0f)
	{
		//**increment MatGlow in positive. DeltaTime is the percentage of a second that has passed. 
		//**Increment by that same percentage of 50.0f
		MatGlow += DeltaTime * 50.0f; 
	}

	//**else if less than 2 seconds
	else if (ElapsedTime <= 2.0f)
	{
		//de-increment MatGlow
		MatGlow -= DeltaTime * 50.0f; 
	}

	//**else more than 2 seconds has passed
	else
	{
		//**reset elapsed time
		ElapsedTime = 0.0f;
	}

	//**clamp the glow
	MatGlow = FMath::Clamp(MatGlow, 1.0f, 50.0f);
	//**set the material glow
	m_dynamicMaterialInstance->SetScalarParameterValue(FName("Glow"), MatGlow);

	//**Get current rotation
	FRotator Rot = GetActorRotation();
	//**Set the Yaw to the DeltaTime percentage of 180, so if completes 1 rotation every 2 seconds
	Rot.Yaw -= DeltaTime * 180.0f; 
	//**Set new rotation
	SetActorRotation(Rot);

}

void ARuntimeCubeActor::PostInitializeComponents()
{

	Super::PostInitializeComponents();

	//**if m_dynamicMaterial successfully initialize
	if (m_dynamicMaterial)
	{
		//**create dynamic material instance
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);

	}

	//**if successfully instanced
	if (m_dynamicMaterialInstance)
	{
		//**set the dynamic material to the plane mesh
		mesh->SetMaterial(0, m_dynamicMaterialInstance);
	}
}
//**override the post actor create
void ARuntimeCubeActor::PostActorCreated()
{
	//**call the function in the super class
	Super::PostActorCreated();

	//**Generate the box
	GenerateBoxMesh();
}

//**overide the post load function
void ARuntimeCubeActor::PostLoad()
{
	//**call the function in the super class
	Super::PostLoad();

	//**Generate the box
	GenerateBoxMesh();
}

void ARuntimeCubeActor::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//**if other actor exists
	if (OtherActor)
	{
		//**If other actor is the player character
		if (OtherActor->IsA(APaintball_COYCharacter::StaticClass()))
		{
			//**Add health
			Cast<APaintball_COYCharacter>(OtherActor)->AddHealth(20.0f);

			//**add score
			GameMode->ChangeScore(200.0f);
			//**Destoyr self
			Destroy();
		}
	}
}




////////////////////////////////////////////////////////////////////////////////////////
///////////////MESH GENERATION//////////////////////////////////////////////////////////
//**generate the box mexh
void ARuntimeCubeActor::GenerateBoxMesh()
{
	//**Array for the vertices of the cube
	TArray<FVector> Vertices;

	//**array for the normals
	TArray<FVector> Normals;

	//**arrray for the tangents
	TArray<FRuntimeMeshTangent> Tangents;

	//**arrya for texture coords
	TArray<FVector2D> TextureCoordinates;

	//**Array for the triangles
	TArray<int32> Triangles;

	//**Array for vertices colors
	TArray<FColor> Colors;

	//**vector for the cube size
	FVector DiamondSize = FVector(25.0f, 25.0f, 50.0f);

	//**call the creat mesh function and pass all the arrays just created
	CreateBoxMesh(DiamondSize, Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors);

	//**create the mesh section and enable collision
	mesh->CreateMeshSection(0, Vertices, Triangles, Normals, TextureCoordinates, Colors, Tangents, true);
}

//**create the box mesh.
void ARuntimeCubeActor::CreateBoxMesh(FVector DiamondSize, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals,
	TArray <FVector2D> & UVs, TArray <FRuntimeMeshTangent> & Tangents, TArray <FColor> & Colors)
{
	//**vector for the box's vertices
	FVector BoxVerts[6];
	BoxVerts[0] = FVector(0, 0, DiamondSize.Z); //**top
	BoxVerts[1] = FVector(DiamondSize.X, 0, 0); //**back
	BoxVerts[2] = FVector(0, DiamondSize.Y, 0); //**right
	BoxVerts[3] = FVector(-DiamondSize.X, 0, 0); //**front
	BoxVerts[4] = FVector(0, -DiamondSize.Y, 0); //**left
	BoxVerts[5] = FVector(0, 0, -DiamondSize.Z); //**bottom

	//**reset the triangle array
	Triangles.Reset();

	//**number of vertices = each face has 3 * 8 faces
	const int32 NumVerts = 24;

	//**reset colors array
	Colors.Reset();

	//**add slots for each vertex
	Colors.AddUninitialized(NumVerts);

	//**Add colors to the vertices in blocks of 3
	for (int i = 0; i < NumVerts / 3; i++)
	{
		Colors[i * 3] = FColor(255, 0, 0);
		Colors[i * 3 + 1] = FColor(0, 255, 0);
		Colors[i * 3 + 2] = FColor(0, 0, 255);
	}

	//**reset and initialize to the number of verts
	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);
	Normals.Reset();
	Normals.AddUninitialized(NumVerts);
	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);

	//**top front left face
	Vertices[0] = BoxVerts[0];
	Vertices[1] = BoxVerts[4];
	Vertices[2] = BoxVerts[3];
	
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);

	Normals[0] = Normals[1] = Normals[2] = FVector(-1, -1, 1); 
	Tangents[0] = Tangents[1] = Tangents[2] = FRuntimeMeshTangent(1.f, 1.f, 1.f);

	//**Left back top face
	Vertices[3] = BoxVerts[0];
	Vertices[4] = BoxVerts[1];
	Vertices[5] = BoxVerts[4];
	
	Triangles.Add(3);
	Triangles.Add(4);
	Triangles.Add(5);

	Normals[3] = Normals[4] = Normals[5] = FVector(1, -1, 1); 
	Tangents[3] = Tangents[4] = Tangents[5] = FRuntimeMeshTangent(-1.f, 1.f, 1.f);

	//**back right top
	Vertices[6] = BoxVerts[0];
	Vertices[7] = BoxVerts[2];
	Vertices[8] = BoxVerts[1];
		
	Triangles.Add(6);
	Triangles.Add(7);
	Triangles.Add(8);

	Normals[6] = Normals[7] = Normals[8] = FVector(1, 1, 1); 
	Tangents[6] = Tangents[7] = Tangents[8] = FRuntimeMeshTangent(-1.f, -1.f, 1.f);

	//**front top right
	Vertices[9] = BoxVerts[0];
	Vertices[10] = BoxVerts[3];
	Vertices[11] = BoxVerts[2];

	Triangles.Add(9);
	Triangles.Add(10);
	Triangles.Add(11);

	Normals[9] = Normals[10] = Normals[11] = FVector(-1, 1, 1);
	Tangents[9] = Tangents[10] = Tangents[11] = FRuntimeMeshTangent(1.f, -1.f, 1.f);

	//**bottom front left
	Vertices[12] = BoxVerts[5];
	Vertices[13] = BoxVerts[3];
	Vertices[14] = BoxVerts[4];
	
	Triangles.Add(12);
	Triangles.Add(13);
	Triangles.Add(14);
	
	Normals[12] = Normals[13] = Normals[14] = FVector(-1, -1, -1); 
	Tangents[12] = Tangents[13] = Tangents[14] = FRuntimeMeshTangent(1.f, 1.f, -1.f);

	//**bottom left back
	Vertices[15] = BoxVerts[5];
	Vertices[16] = BoxVerts[4];
	Vertices[17] = BoxVerts[1];

	Triangles.Add(15);
	Triangles.Add(16);
	Triangles.Add(17);
	
	Normals[15] = Normals[16] = Normals[17] = FVector(1, -1, -1); 
	Tangents[15] = Tangents[16] = Tangents[17] = FRuntimeMeshTangent(-1.f, 1.f, -1.f);

	//**bottom right back
	Vertices[18] = BoxVerts[5];
	Vertices[19] = BoxVerts[1];
	Vertices[20] = BoxVerts[2];

	Triangles.Add(18);
	Triangles.Add(19);
	Triangles.Add(20);

	Normals[18] = Normals[19] = Normals[20] = FVector(1, 1, -1);
	Tangents[18] = Tangents[19] = Tangents[20] = FRuntimeMeshTangent(-1.f, -1.f, -1.f);

	//**bottom right front
	Vertices[21] = BoxVerts[5];
	Vertices[22] = BoxVerts[2];
	Vertices[23] = BoxVerts[3];

	Triangles.Add(21);
	Triangles.Add(22);
	Triangles.Add(23);

	Normals[21] = Normals[22] = Normals[23] = FVector(-1, 1, -1);
	Tangents[21] = Tangents[22] = Tangents[23] = FRuntimeMeshTangent(1.f, -1.f, -1.f);


	//**set the UVs
	UVs.Reset();
	UVs.AddUninitialized(NumVerts);
	UVs[0] = UVs[4] = UVs[8] = UVs[12] = UVs[16] = UVs[20] = FVector2D(0.f, 0.f);
	UVs[1] = UVs[5] = UVs[9] = UVs[13] = UVs[17] = UVs[21] = FVector2D(0.f, 1.f);
	UVs[2] = UVs[6] = UVs[10] = UVs[14] = UVs[18] = UVs[22] = FVector2D(1.f, 1.f);
	UVs[3] = UVs[7] = UVs[11] = UVs[15] = UVs[19] = UVs[23] = FVector2D(1.f, 0.f);

}

