// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Classes/Engine/TextureRenderTarget2D.h"
#include "Classes/Engine/World.h"
#include "Public/GlobalShader.h"
#include "Public/PipelineStateCache.h"
#include "Public/RHIStaticStates.h"
#include "Public/SceneUtils.h"
#include "Public/SceneInterface.h"
#include "Public/ShaderParameterUtils.h"
#include "SquareActor.generated.h"

UCLASS()
class PAINTBALL_COY_API ASquareActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASquareActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	//**the procedural mesh component object
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent *mesh;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//** override the post actor created method
	virtual void PostActorCreated() override;

	//** over ride the post load method
	virtual void PostLoad() override;

	//** create the square
	virtual void CreateSquare();




	
	
};
