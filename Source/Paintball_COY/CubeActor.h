// Fill out your copyright notice in the Description page of Project Settings.

/////////////////////////////////Cube Obstacle//////////////////////////////////////

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "Paintball_COYGameMode.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "ProceduralMeshComponent.h"
#include "CubeActor.generated.h"

UCLASS()
class PAINTBALL_COY_API ACubeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACubeActor();
   
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	//**procedural mesh component
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent *mesh;

	//**texture size
	static const int m_textureSize = 512;

	//**2d texture
	UPROPERTY()
	UTexture2D *m_dynamicTexture;

	//**material interface
	UPROPERTY()
	UMaterialInterface *m_dynamicMaterial;

	//**dynamic material instance
	UPROPERTY()
	UMaterialInstanceDynamic *m_dynamicMaterialInstance;

	//**material interface
	UPROPERTY()
	UMaterialInterface *m_dynamicMaterial_Solid;

	//**dynamic material instance
	UPROPERTY()
	UMaterialInstanceDynamic *m_dynamicMaterialInstance_Solid;

	//**reference to the game mode
	UPROPERTY()
	class APaintball_COYGameMode *GameMode;

	//**cover size
	float m_coverSize;

	//**Color for the material
	FLinearColor MatColor;

	//**Material opacity
	float MatOpacity;

	//**material glow
	float MatGlow; 

	//**Count the number of hits taken
	int8 numHits; 
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//**function to generate the cube
	virtual void GenerateBoxMesh();

	//**create the box mesh
	virtual void CreateBoxMesh(FVector BoxRadius, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals, 
		TArray <FVector2D> & UVs, TArray <FProcMeshTangent> & Tangents, TArray <FColor> & Colors);

	//**over ride the post create actor function
	virtual void PostActorCreated() override;

	//** override the post load function
	virtual void PostLoad() override;

	//**Overridden event after the component is initialized
	virtual void PostInitializeComponents() override;

	/** called when hit*/
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	
};
