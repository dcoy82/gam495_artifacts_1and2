// Fill out your copyright notice in the Description page of Project Settings.

#include "GeneratedBoxActor.h"


// Sets default values
AGeneratedBoxActor::AGeneratedBoxActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//**intialize the mesh component
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));

	//**set the mesh as the root component
	RootComponent = mesh;
	
	//**Set the material for the mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> Glass(TEXT("/Game/Materials/M_GeneratedBoxGlass"));
	
	if (Glass.Succeeded())
	{
		m_dynamicMaterial = Glass.Object;
	}

	//**set initial opacity, used for making the box fade out
	MatOpacity = 1.0f; 

	//**initialize is placed toggle
	isPlaced = false; 	
}

void AGeneratedBoxActor::PostInitializeComponents()
{
	//**call the super class function
	Super::PostInitializeComponents();

	//**if m_dynamicMaterial successfully initialize
	if (m_dynamicMaterial)
	{
		//**create dynamic material instance
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);

	}

	//**if successfully instanced
	if (m_dynamicMaterialInstance)
	{
		//**set the dynamic material to the plane mesh
		mesh->SetMaterial(0, m_dynamicMaterialInstance);
	}

	//**set transclucency prioryt
	mesh->SetTranslucentSortPriority(10);
}

// Called when the game starts or when spawned
void AGeneratedBoxActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGeneratedBoxActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//**if lifespace less that 2 and box has been placed
	if (GetLifeSpan() < 2.0f && isPlaced)
	{
		//**DeltaTime/2.0f will equal the percentage of the last 2 seconds of life that has passed. 
		//Subtract that from the material opactiry to have the material fade evening over the life 
		MatOpacity -= DeltaTime / 2.0f;

		//**Set the color parameter to the new value
		m_dynamicMaterialInstance->SetScalarParameterValue(FName(TEXT("Opacity")), MatOpacity);
	}

}

//**override the post actor create
void AGeneratedBoxActor::PostActorCreated()
{
	//**call the function in the super class
	Super::PostActorCreated();

	//**Generate the box
	GenerateBoxMesh();
}

//**overide the post load function
void AGeneratedBoxActor::PostLoad()
{
	//**call the function in the super class
	Super::PostLoad();

	//**Generate the box
	GenerateBoxMesh();
}

//**Function for changing the material between the clear and solid
void AGeneratedBoxActor::SetMaterialClear(bool clear)
{
	//**if the mesh has been set
	if (mesh != NULL)
	{
		//**if clear is true, set the material to the clear version
		if (clear)
		{
			m_dynamicMaterialInstance->SetScalarParameterValue(FName(TEXT("Opacity")), 0.1f);
			m_dynamicMaterialInstance->SetScalarParameterValue(FName(TEXT("Specular")), 1.0f);
			m_dynamicMaterialInstance->SetScalarParameterValue(FName(TEXT("Roughness")), 0.0f);
			m_dynamicMaterialInstance->SetVectorParameterValue(FName(TEXT("ColorGlass")), FLinearColor(0.2f, 0.7f, 0.9f, 1.0f));

		}

		//**else set it to the solid verion
		else
		{
			m_dynamicMaterialInstance->SetScalarParameterValue(FName(TEXT("Opacity")), 1.0f);
			m_dynamicMaterialInstance->SetScalarParameterValue(FName(TEXT("Specular")), 0.0f);
			m_dynamicMaterialInstance->SetScalarParameterValue(FName(TEXT("Roughness")), 1.0f);
			m_dynamicMaterialInstance->SetVectorParameterValue(FName(TEXT("ColorGlass")), FLinearColor(0.1f, 0.3f, 1.0f, 1.0f));

			//**set toggle to placed
			isPlaced = true; 

			//**set the life span for 10 seconds
			SetLifeSpan(10.0f);

		}
	}
}


//**generate the box mexh
void AGeneratedBoxActor::GenerateBoxMesh()
{
	//**Array for the vertices of the cube
	TArray<FVector> Vertices;

	//**array for the normals
	TArray<FVector> Normals;

	//**arrray for the tangents
	TArray<FProcMeshTangent> Tangents;

	//**arrya for texture coords
	TArray<FVector2D> TextureCoordinates;

	//**Array for the triangles
	TArray<int32> Triangles;

	//**Array for vertices colors
	TArray<FColor> Colors;

	//**vector for the cube size
	FVector CubeSize = FVector(100.0f, 100.0f, 35.0f);

	//**call the creat mesh function and pass all the arrays just created
	CreateBoxMesh(CubeSize, Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors);

	//**create the mesh section and enable collision
	mesh->CreateMeshSection(0, Vertices, Triangles, Normals, TextureCoordinates, Colors, Tangents, true);
}

//**create the box mesh.
void AGeneratedBoxActor::CreateBoxMesh(FVector BoxRadius, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals,
	TArray <FVector2D> & UVs, TArray <FProcMeshTangent> & Tangents, TArray <FColor> & Colors)
{
	//**vector array for the box's vertices
	FVector BoxVerts[8];
	BoxVerts[0] = FVector(-BoxRadius.X, BoxRadius.Y, BoxRadius.Z);
	BoxVerts[1] = FVector(BoxRadius.X, BoxRadius.Y, BoxRadius.Z);
	BoxVerts[2] = FVector(BoxRadius.X, -BoxRadius.Y, BoxRadius.Z);
	BoxVerts[3] = FVector(-BoxRadius.X, -BoxRadius.Y, BoxRadius.Z);
	BoxVerts[4] = FVector(-BoxRadius.X, BoxRadius.Y, -BoxRadius.Z);
	BoxVerts[5] = FVector(BoxRadius.X, BoxRadius.Y, -BoxRadius.Z);
	BoxVerts[6] = FVector(BoxRadius.X, -BoxRadius.Y, -BoxRadius.Z);
	BoxVerts[7] = FVector(-BoxRadius.X, -BoxRadius.Y, -BoxRadius.Z);

	//**reset the triangle array
	Triangles.Reset();

	//**number of vertices = each face has 4 * 6 faces on a cube
	const int32 NumVerts = 24;

	//**reset colors array
	Colors.Reset();

	//**add slots for each vertex
	Colors.AddUninitialized(NumVerts);

	//**Add colors to the vertices in blocks of 3
	for (int i = 0; i < NumVerts / 3; i++)
	{
		Colors[i * 3] = FColor(255, 0, 0);
		Colors[i * 3 + 1] = FColor(0, 255, 0);
		Colors[i * 3 + 2] = FColor(0, 0, 255);
	}

	//**reset and initialize to the number of verts
	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);
	Normals.Reset();
	Normals.AddUninitialized(NumVerts);
	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);

	//**Top face of the cube
	Vertices[0] = BoxVerts[0];
	Vertices[1] = BoxVerts[1];
	Vertices[2] = BoxVerts[2];
	Vertices[3] = BoxVerts[3];
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(3);
	Triangles.Add(1);
	Triangles.Add(2);
	Triangles.Add(3);
	Normals[0] = Normals[1] = Normals[2] = Normals[3] = FVector(0, 0, 1); //**This normal points up along z-axis
	Tangents[0] = Tangents[1] = Tangents[2] = Tangents[3] = FProcMeshTangent(0.f, -1.f, 0.f);

	//**left face of cube
	Vertices[4] = BoxVerts[4];
	Vertices[5] = BoxVerts[0];
	Vertices[6] = BoxVerts[3];
	Vertices[7] = BoxVerts[7];
	Triangles.Add(4);
	Triangles.Add(5);
	Triangles.Add(7);
	Triangles.Add(5);
	Triangles.Add(6);
	Triangles.Add(7);
	Normals[4] = Normals[5] = Normals[6] = Normals[7] = FVector(-1, 0, 0); //**this normal points left along x-axis
	Tangents[4] = Tangents[5] = Tangents[6] = Tangents[7] = FProcMeshTangent(0.f, -1.f, 0.f);

	//**front face
	Vertices[8] = BoxVerts[5];
	Vertices[9] = BoxVerts[1];
	Vertices[10] = BoxVerts[0];
	Vertices[11] = BoxVerts[4];
	Triangles.Add(8);
	Triangles.Add(9);
	Triangles.Add(11);
	Triangles.Add(9);
	Triangles.Add(10);
	Triangles.Add(11);
	Normals[8] = Normals[9] = Normals[10] = Normals[11] = FVector(0, 1, 0); //**this normal points forward along y-axis 
	Tangents[8] = Tangents[9] = Tangents[10] = Tangents[11] = FProcMeshTangent(-1.f, 0.f, 0.f);

	//**right face
	Vertices[12] = BoxVerts[6];
	Vertices[13] = BoxVerts[2];
	Vertices[14] = BoxVerts[1];
	Vertices[15] = BoxVerts[5];
	Triangles.Add(12);
	Triangles.Add(13);
	Triangles.Add(15);
	Triangles.Add(13);
	Triangles.Add(14);
	Triangles.Add(15);
	Normals[12] = Normals[13] = Normals[14] = Normals[15] = FVector(1, 0, 0); //**this normal points right along x-axis
	Tangents[12] = Tangents[13] = Tangents[14] = Tangents[15] = FProcMeshTangent(0.f, 1.f, 0.f);

	//**rear face
	Vertices[16] = BoxVerts[7];
	Vertices[17] = BoxVerts[3];
	Vertices[18] = BoxVerts[2];
	Vertices[19] = BoxVerts[6];
	Triangles.Add(16);
	Triangles.Add(17);
	Triangles.Add(19);
	Triangles.Add(17);
	Triangles.Add(18);
	Triangles.Add(19);
	Normals[16] = Normals[17] = Normals[18] = Normals[19] = FVector(0, -1, 0); //**this normal points back along y-axis
	Tangents[16] = Tangents[17] = Tangents[18] = Tangents[19] = FProcMeshTangent(1.f, 0.f, 0.f);

	//**bottom face
	Vertices[20] = BoxVerts[7];
	Vertices[21] = BoxVerts[6];
	Vertices[22] = BoxVerts[5];
	Vertices[23] = BoxVerts[4];
	Triangles.Add(20);
	Triangles.Add(21);
	Triangles.Add(23);
	Triangles.Add(21);
	Triangles.Add(22);
	Triangles.Add(23);
	Normals[20] = Normals[21] = Normals[22] = Normals[23] = FVector(0, 0, -1); //**this normal points down along z-axis
	Tangents[20] = Tangents[21] = Tangents[22] = Tangents[23] = FProcMeshTangent(0.f, 1.f, 0.f);

	//**set the UVs
	UVs.Reset();
	UVs.AddUninitialized(NumVerts);
	UVs[0] = UVs[4] = UVs[8] = UVs[12] = UVs[16] = UVs[20] = FVector2D(0.f, 0.f);
	UVs[1] = UVs[5] = UVs[9] = UVs[13] = UVs[17] = UVs[21] = FVector2D(0.f, 1.f);
	UVs[2] = UVs[6] = UVs[10] = UVs[14] = UVs[18] = UVs[22] = FVector2D(1.f, 1.f);
	UVs[3] = UVs[7] = UVs[11] = UVs[15] = UVs[19] = UVs[23] = FVector2D(1.f, 0.f);

}



