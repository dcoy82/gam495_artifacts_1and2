// Fill out your copyright notice in the Description page of Project Settings.

#include "SquareActor.h"



// Sets default values
ASquareActor::ASquareActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//**initialize the mesh object
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));

	//** set the mesh as the root component
	RootComponent = mesh;

}

// Called when the game starts or when spawned
void ASquareActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASquareActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//**Post actor created
void ASquareActor::PostActorCreated()
{
	//**call the function in the super class
	Super::PostActorCreated();

	//**Create the square
	CreateSquare();
}

//**post load function
void ASquareActor::PostLoad()
{
	//**call the super class function
	Super::PostLoad();

	CreateSquare();

}

//**create the swuare
void ASquareActor::CreateSquare()
{
	//**array of vector to hold the square's vertices
	TArray<FVector> Vertices;

	//**int array to hold the the rendering triangles
	TArray<int32> Triangles;

	//**vector array to hold the vertices' normals
	TArray<FVector> Normals;

	//**array to hold the vertices' colors
	TArray<FLinearColor> Colors;

	
	//**add the sqaure's vertices to the array
	Vertices.Add(FVector(0.0f, 0.0f, 0.0f));
	Vertices.Add(FVector(0.0f, 200.0f, 0.0f));
	Vertices.Add(FVector(0.0f, 0.0f, 100.0f));
	Vertices.Add(FVector(0.0f, 200.0f, 100.0f));
	Vertices.Add(FVector(0.0f, -100.0f, 50.0f));

	//**add the triangles
	//**first triangle is vertex 0->1->2
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
	//**second triangle is vertex 3->2->1
	Triangles.Add(3);
	Triangles.Add(2);
	Triangles.Add(1);

	//**third trianle is vertext 0->2->4
	Triangles.Add(0);
	Triangles.Add(2);
	Triangles.Add(4);

	//**for each vertex in Vertices, add a Normal and Color
	for (int32 i = 0; i < Vertices.Num(); i++)
	{
		//**give the vertext a normal along z axis
		Normals.Add(FVector(0.0f, 0.0f, 1.0f));
		//**give the vertex a random color
		Colors.Add(FLinearColor::MakeRandomColor());
	}

	//optional arrays
	TArray<FVector2D> UVO;
	TArray<FProcMeshTangent> Tangents;

	//**create the mesh
	mesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UVO, Colors, Tangents, true);
	
	
}

