// Fill out your copyright notice in the Description page of Project Settings.

////////////////////////HEALTHPACK///////////////////////

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine.h"
#include "Paintball_COYCharacter.h"
#include "RuntimeMeshComponent.h"
#include "RuntimeCubeActor.generated.h"

UCLASS()
class PAINTBALL_COY_API ARuntimeCubeActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARuntimeCubeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	//**procedural mesh component
	UPROPERTY(VisibleAnywhere)
	URuntimeMeshComponent *mesh;

	//**material interface
	UPROPERTY()
	UMaterialInterface *m_dynamicMaterial;

	//**dynamic material instance
	UPROPERTY()
	UMaterialInstanceDynamic *m_dynamicMaterialInstance;

	//**reference to the game mode
	UPROPERTY()
	class APaintball_COYGameMode *GameMode;

	//**Color for the material
	FLinearColor MatColor;

	//**Material opacity
	float MatOpacity;

	//**material glow
	float MatGlow;

	//**track the elapsed time in the tick
	float ElapsedTime;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//**function to generate the cube
	virtual void GenerateBoxMesh();

	//**create the box mesh
	virtual void CreateBoxMesh(FVector BoxRadius, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals,
		TArray <FVector2D> & UVs, TArray <FRuntimeMeshTangent> & Tangents, TArray <FColor> & Colors);

	//**over ride the post create actor function
	virtual void PostActorCreated() override;

	//** override the post load function
	virtual void PostLoad() override;

	//**Overridden event after the component is initialized
	virtual void PostInitializeComponents() override;

	/** called when hit*/
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


};
