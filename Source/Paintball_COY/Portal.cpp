
// Fill out your copyright notice in the Description page of Project Settings.

#include "Portal.h"
#include "Paintball_COYCharacter.h"
#include "Paintball_COYProjectile.h"


// Sets default values
APortal::APortal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//**portal is initially inactive
	isActive = false;

	//**initialize transport coords
	TransportCoords = FVector(0.0f, 0.0f, 0.0f);

	//**initialize coords toggle to false
	coordsSet = false; 

	//**Create the static mesh component
	CylinderMeshOne = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cylinder1"));
			
	//**Find the 3d model asset for the box in the starter content folders
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CylinderVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cylinder.Shape_Cylinder"));
	//**If the visual asset was successfully found, then set its parameters
	if (CylinderVisualAsset.Succeeded())
	{
		//**Set the sphere mesh to the visual asset we just found
		CylinderMeshOne->SetStaticMesh(CylinderVisualAsset.Object);
		
		//**Set the relative location of the cylinder
		CylinderMeshOne->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));

		CylinderMeshOne->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
		
		//**Set the scale size of the cylinder
		CylinderMeshOne->SetWorldScale3D(FVector(2.0f, 2.0f, 0.1f));
		
	}

	//**set collision
	CylinderMeshOne->BodyInstance.SetCollisionProfileName(TEXT("BlockAllDynamic"));

	RootComponent = CylinderMeshOne;

	//**Set the material for the mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> PortalMaterial(TEXT("/Game/Materials/M_Black"));
	if (PortalMaterial.Succeeded())
	{
		PortalMat = PortalMaterial.Object; 
		CylinderMeshOne->SetMaterial(0, PortalMat);
	}

	//**register the OnHit function
	CylinderMeshOne->OnComponentHit.AddDynamic(this, &APortal::OnHit);
}

// Called when the game starts or when spawned
void APortal::BeginPlay()
{
	Super::BeginPlay();

	//**Get game mode
	GameMode = Cast<APaintball_COYGameMode>(GetWorld()->GetAuthGameMode());

	//**Get the playey character
	PlayerChar = Cast<APaintball_COYCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	
}

// Called every frame
void APortal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//**Rotate towards the player, so they always see the face of the portal
	//**NOT IS USE. Previously used when portals were placed statically in level
	//**to have thema always facing the player
	//RotateToPlayer();
	
	
	//**Add the delta time to elapsed time
	ElapsedTime += DeltaTime;	
	
	//**If elapsed time has exceed 15 seconds, then deactivate the portal
	if (ElapsedTime >= 10.0f && isActive)
	{
		//**set to deactive
		isActive = false;
			
		//**Show UI message
		GameMode->SetUIMessage("Portal Deactivated");		
		
		//**set the player's portal active toggle to false
		PlayerChar->PortalActive = false;

		//**destroy portal
		Destroy();
	}
	
}

//**on hit
void APortal::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//**if the OtherActor isn't NULL
	if (OtherActor != NULL)
	{		
		//**if other actor is the player, and portal is active, and transport coords have been set
		if (OtherActor->IsA(APaintball_COYCharacter::StaticClass()) && isActive && coordsSet)
		{

			//**add to score
			GameMode->ChangeScore(50.0f);

			//**Transport the player to the TransportCoords 
			OtherActor->SetActorLocation(TransportCoords, false, (FHitResult *)nullptr, ETeleportType::TeleportPhysics); 

			//**Set the player rotation to match the exit portal's
			PlayerChar->GetController()->SetControlRotation(TransportRotation);
		}
	}
}

//**Set portal to active
void APortal::SetActive()
{
	isActive = true; 

	//**Show UI message
	GameMode->SetUIMessage("Portal Activated");
}

bool APortal::CheckActive()
{
	return isActive;
}

//**This function is not currently in use. When portals were previously statically placed in the level,
//**this function was used to keep them alwasy rotated towards the player. 
//**Rotate the portal towards the player, so they always see its face
void APortal::RotateToPlayer()
{

	//**Get the player's location vector
	FVector PlayerLoc; 
	if (PlayerChar)
	{
		PlayerLoc = PlayerChar->GetActorLocation();
	}

	else
	{
		PlayerLoc = FVector(0, 0, 0);
	}

	//**Get the portal location 
	FVector PortalLoc = GetActorLocation();

	//**Only continue if within certain distance. Don't need to rotate if out of view
	if (FVector::Dist(PlayerLoc, PortalLoc) < 2000)
	{

		//**The directional vector from the portal to the player, is the portal location minus the player location
		FVector Direction = PortalLoc - PlayerLoc;

		//**Get the current rotaion of the portal
		FRotator NewRotation = GetActorRotation();

		//**We're only rotating around the Z-axis, so only need to change the yaw. 
		//**Set the new yaw to the yaw found in the Direction vector. Add 90 to compensate for the static mesh's rotation
		NewRotation.Yaw = Direction.Rotation().Yaw + 90.0f;

		//**Set the new rotation
		SetActorRotation(NewRotation);
	}
}

//**set the transport coords and rotation
void APortal::SetTransCoords(FVector Coords, FRotator Rotation)
{
	if (!Coords.IsZero())
	{
		TransportCoords = Coords;

		TransportRotation = Rotation;

		//**set coords set
		coordsSet = true;
	}
}

//**get the transport coords
FVector APortal::GetTransCoords()
{
	return TransportCoords;

}

