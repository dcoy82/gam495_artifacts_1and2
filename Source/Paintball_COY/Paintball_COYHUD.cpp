// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_COYHUD.h"
#include "Engine.h"
#include "Engine/Canvas.h"
#include "CanvasItem.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"

APaintball_COYHUD::APaintball_COYHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;

	//**Set the ui widget
	static ConstructorHelpers::FClassFinder<UUserWidget> UIObj(TEXT("/Game/UI/BP_UI"));
	HUDWidgetClass = UIObj.Class;
}


void APaintball_COYHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

void APaintball_COYHUD::BeginPlay()
{
	Super::BeginPlay();

	//**If HUD Widget was succesfully loaded
	if (HUDWidgetClass)
	{
		//**Create the widget
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);
				
		//**If successful
		if (CurrentWidget)
		{
			//**Add widget to screen
			CurrentWidget->AddToViewport();
		}
	}

}
