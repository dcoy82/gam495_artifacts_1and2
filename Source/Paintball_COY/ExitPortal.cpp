// Fill out your copyright notice in the Description page of Project Settings.

#include "ExitPortal.h"


// Sets default values
AExitPortal::AExitPortal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//**Create the static mesh component
	CylinderMeshOne = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cylinder2"));

	//**Find the 3d model asset for the box in the starter content folders
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CylinderVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cylinder.Shape_Cylinder"));
	//**If the visual asset was successfully found, then set its parameters
	if (CylinderVisualAsset.Succeeded())
	{
		//**Set the sphere mesh to the visual asset we just found
		CylinderMeshOne->SetStaticMesh(CylinderVisualAsset.Object);

		//**Set the relative location of the cylinder
		CylinderMeshOne->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));

		CylinderMeshOne->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));

		//**Set the scale size of the cylinder
		CylinderMeshOne->SetWorldScale3D(FVector(2.0f, 2.0f, 0.1f));

	}

	//**set collision
	CylinderMeshOne->BodyInstance.SetCollisionProfileName(TEXT("BlockAllDynamic"));

	RootComponent = CylinderMeshOne;

	//**Set the material for the mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> PortalMaterial(TEXT("/Game/Materials/M_ExitPortal"));
	if (PortalMaterial.Succeeded())
	{
		PortalMat = PortalMaterial.Object;
		CylinderMeshOne->SetMaterial(0, PortalMat);
	}


}

// Called when the game starts or when spawned
void AExitPortal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AExitPortal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//**Add the time that's passed to the total elapsed time
	ElapsedTime += DeltaTime;

	//**When total elapsed time reaches 10 seconds
	if (ElapsedTime >= 10.0f)
	{
		//**destroy the exit portal
		Destroy();
	}

}

