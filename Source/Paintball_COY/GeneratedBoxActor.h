// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Runtime/CoreUObject//Public/UObject/ConstructorHelpers.h"
#include "GeneratedBoxActor.generated.h"

UCLASS()
class PAINTBALL_COY_API AGeneratedBoxActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGeneratedBoxActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	//**procedural mesh component
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent *mesh;

	//**Material interface
	UPROPERTY()
	UMaterialInterface *m_dynamicMaterial;

	//UMaterialInterface *SolidMat;

	//**dynamic material instance
	UPROPERTY()
	UMaterialInstanceDynamic *m_dynamicMaterialInstance;
	
	//**control material opacity
	float MatOpacity;

	//**toggle for when box has been placed
	bool isPlaced; 

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//**function to generate the cube
	virtual void GenerateBoxMesh();

	//**create the box mesh
	virtual void CreateBoxMesh(FVector BoxRadius, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals,
		TArray <FVector2D> & UVs, TArray <FProcMeshTangent> & Tangents, TArray <FColor> & Colors);

	//**over ride the post create actor function
	virtual void PostActorCreated() override;

	//** override the post load function
	virtual void PostLoad() override;

	//**Function to set the object's material to clear or solid
	void SetMaterialClear(bool clear);

	//**Overridden event after the component is initialized
	virtual void PostInitializeComponents() override;	
	
};
