// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_COYCharacter.h"
#include "Paintball_COYProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Portal.h"
#include "ExitPortal.h"
#include "SplatActor.h"
#include "MotionControllerComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// APaintball_COYCharacter

APaintball_COYCharacter::APaintball_COYCharacter()
{
	//**enable tick event
	PrimaryActorTick.bCanEverTick = true; 

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	
	FP_Gun->SetupAttachment(RootComponent);
	
	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
	
	//**Set the offset for spawning boxes
	GeneratedBoxOffset = FVector(300.0f, 0.0f, 0.0f);

	//**Set the toggle for the secondary fire button
	SecondaryFireIsPressed = false; 
	

	//**set health and stamina
	Health = 100.0f;
	Stamina = 100.0f;
	ElapsedTime = 0.0f; 
	PortalActive = false;
	   
	//****Mod 5 tutorial shader variables
	EndColorBuildup = 0;
	EndColorBuildupDirection = 1;
	PixelShaderTopLeftColor = FColor::Green;
	ComputeShaderSimulationSpeed = 1.0f;
	ComputeShaderBlend = 0.5f;
	ComputeShaderBlendScalar = 5.0f;
	TotalElapedTime = 0;
	
}

void APaintball_COYCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}
	
	//**Mod 5 tutorial
	//**instantiate PixelShading and ComputeShading
	PixelShading = new FPixelShaderUsageExample(PixelShaderTopLeftColor, GetWorld()->Scene->GetFeatureLevel());

	ComputeShading = new FComputeShaderUsageExample(ComputeShaderSimulationSpeed, 1024, 1024, GetWorld()->Scene->GetFeatureLevel());

	//**Get the game mode
	GameMode = Cast<APaintball_COYGameMode>(GetWorld()->GetAuthGameMode());

}

//**Begin destroy function
void APaintball_COYCharacter::BeginDestroy()
{
	//**Call the super class's function
	Super::BeginDestroy();

	//**if PixelShading exists, then delete
	if (PixelShading)
	{
		delete PixelShading;
	}

	//**If ComputeShading exists, then delete
	if (ComputeShading)
	{
		delete ComputeShading;
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void APaintball_COYCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APaintball_COYCharacter::OnFire);

	//**Bind secondary fire
	PlayerInputComponent->BindAction("SecondaryFire", IE_Pressed, this, &APaintball_COYCharacter::OnSecondaryFirePressed);
	PlayerInputComponent->BindAction("SecondaryFire", IE_Released, this, &APaintball_COYCharacter::OnSecondaryFireReleased);

	//**Bind tertiary fire
	PlayerInputComponent->BindAction("TertiaryFire", IE_Pressed, this, &APaintball_COYCharacter::OnTertiaryFirePressed);
	PlayerInputComponent->BindAction("TertiaryFire", IE_Released, this, &APaintball_COYCharacter::OnTertiaryFireReleased);

	//**Bind mouse scroll up
	PlayerInputComponent->BindAction("ScrollUp", IE_Pressed, this, &APaintball_COYCharacter::OnScrollUp);

	//**Bind mouse scroll down
	PlayerInputComponent->BindAction("ScrollDown", IE_Pressed, this, &APaintball_COYCharacter::OnScrollDown);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &APaintball_COYCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &APaintball_COYCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APaintball_COYCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &APaintball_COYCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &APaintball_COYCharacter::LookUpAtRate);

	//**Shader plugin input mappings
	PlayerInputComponent->BindAction("SavePixelShaderOutput", IE_Pressed, this, &APaintball_COYCharacter::SavePixelShaderOutput);
	PlayerInputComponent->BindAction("SaveComputeShaderOutput", IE_Pressed, this, &APaintball_COYCharacter::SaveComputeShaderOutput);
	PlayerInputComponent->BindAxis("ComputeShaderBlend", this, &APaintball_COYCharacter::ModifyComputeShaderBlend);
}


//**Tick event
// Called every frame
void APaintball_COYCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//**Add the delta time to the ElapsedTime for stamina regeneration
	ElapsedTime += DeltaTime;

	//**Add the delta time to the TotalElapsedTime for shader calcualtions
	TotalElapedTime += DeltaTime;

	//**Show health and stamina 

	//**If the player is holding secondary fire, and there is a generated box
	if (SecondaryFireIsPressed && GeneratedBox != NULL)
	{
		//**Set the location for the generated box to be the player location, plus the offset rotated to match the control rotation
		//** this will set the location to be straight out from where the player is looking

		GeneratedBox->SetActorLocation(GetActorLocation() + (GetControlRotation().RotateVector(GeneratedBoxOffset)));

		FRotator BoxRotation = GetControlRotation();
		BoxRotation.Pitch = 0.0f;
		GeneratedBox->SetActorRotation(BoxRotation);
	}

	//**if Elapsed time has reach 1 second and Stamina is less than 100
	if (ElapsedTime >= 1.0f)
	{
		if (Stamina < 100.0f)
		{
			//**regenerate a small amount
			Stamina += 2.0f;
		}

		//**Reduce health
		Health -= 1.0f;
		
		//**clamp health
		Health = FMath::Clamp(Health, 0.0f, 100.0f);

		//**reset ElapsedTime
		ElapsedTime = 0.0f;
	}

	//*********************Module 5 tutorial shaders*****************************
	//**if PixelShading exists
	if (PixelShading)
	{
		//**incrementing EndColorBuildup by the amount of time that has passed in either the positive of negative direction, clamped to between 0 and 1
		EndColorBuildup = FMath::Clamp(EndColorBuildup + DeltaTime * EndColorBuildupDirection, 0.0f, 1.0f);
	}

	//**if EndColorBuildup has reached 1 or 0, then switch the direction
	if (EndColorBuildup >= 1.0f || EndColorBuildup <= 0.0f)
	{
		EndColorBuildupDirection *= -1;
	}

	//**Create the input texture reference
	FTexture2DRHIRef InputTexture = NULL;

	//**if ComputeShading exists
	if (ComputeShading)
	{
		//**Execute 
		ComputeShading->ExecuteComputeShader(TotalElapedTime);

		//**Get the texture that was just computed
		InputTexture = ComputeShading->GetTexture();
	}

	//**Set the shader blend based on the time that has elapsed
	ComputeShaderBlend = FMath::Clamp(ComputeShaderBlend + ComputeShaderBlendScalar * DeltaTime, 0.0f, 1.0f);

	//**exectute the pixel shading
	PixelShading->ExecutePixelShader(RenderTarget, InputTexture, FColor(EndColorBuildup * 255, 0, 0, 255), ComputeShaderBlend);
}

//**On press fire
void APaintball_COYCharacter::OnFire()
{
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<APaintball_COYProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
					
				// spawn the projectile at the muzzle
				World->SpawnActor<APaintball_COYProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
		
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

//**Secondary fire press
void APaintball_COYCharacter::OnSecondaryFirePressed()
{	
	//((If player has enough stamina
	if (Stamina >= 20.0f)
	{
		//**Get the current control rotation, ie where the player is looking
		FRotator SpawnRotation = GetControlRotation();

		//**Set the location for spawning to be the player location, plus the offset rotated to match the control rotation
		//** this will set the location to be straight out from where the player is looking
		FVector SpawnLocation = GetActorLocation() + (SpawnRotation.RotateVector(GeneratedBoxOffset));

		//**Spawn a AGeneratedBoxActor object in the world
		GeneratedBox = GetWorld()->SpawnActor<AGeneratedBoxActor>(AGeneratedBoxActor::StaticClass(), SpawnLocation, SpawnRotation);

		//**Set the spawn rotation pitch to zero, then set the generated box object's rotation. This makes it so the box is horizontal
		SpawnRotation.Pitch = 0.0f;
		GeneratedBox->SetActorRotation(SpawnRotation);

		//**Set the is pressed toggle to true
		SecondaryFireIsPressed = true;

		//**decrease stamina
		Stamina -= 20.0f;
	}
}

//**secondary fire release
void APaintball_COYCharacter::OnSecondaryFireReleased()
{
	//**if currently holding a box
	if (GeneratedBox != NULL)
	{
		//**set the material to solid
		GeneratedBox->SetMaterialClear(false);
		
		//**Clear out the GeneratedBox pointer, so the player can no longer change its location
		GeneratedBox = NULL;
	}

	//**Reset the offset for spawning boxes
	GeneratedBoxOffset = FVector(300.0f, 0.0f, 0.0f);

	//**Set the is pressed toggle to false
	SecondaryFireIsPressed = false;
}

//**mouse wheel scroll up
void APaintball_COYCharacter::OnScrollUp()
{
	//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Cyan, "Scroll Up");

	//**If the player is holding the secondary fire button AND there is a Generated Box
	if (SecondaryFireIsPressed && GeneratedBox != NULL)
	{
		//**Push the off set out further
		GeneratedBoxOffset += FVector(50.0f, 0.0f, 0.0f);
		//**if try to scroll passed limit, reset to limit
		if (GeneratedBoxOffset.X > 1000.0)
		{
			GeneratedBoxOffset.X = 1000.0f;
		}

	}
}

//**mouse wheel scroll down
void APaintball_COYCharacter::OnScrollDown()
{
	//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Cyan, "Scroll Down");

	//**If the player is holding the secondary fire button AND there is a Generated Box
	if (SecondaryFireIsPressed && GeneratedBox != NULL)
	{	
		//**Pull the offset in
		GeneratedBoxOffset -= FVector(50.0f, 0.0f, 0.0f);

		//**if try to scroll passed limit, reset to limit
		if (GeneratedBoxOffset.X < 200.0)
		{
			GeneratedBoxOffset.X = 200.0f;
		}
	}
}


//**Tertiary Fire...middle mouse button
void APaintball_COYCharacter::OnTertiaryFirePressed()
{
	//**Hit result for the line trace
	FHitResult HitResult;

	//**Start location of the line trace is the muzzle, same location the projectile spawns
	FVector StartLocation = GetActorLocation() + GetControlRotation().RotateVector(FVector(70.0f, 10.0f, 10.0f)); //FirstPersonCameraComponent->GetComponentLocation();

	//**line trace direct is the rotation of the camera
	FRotator Direction = GetControlRotation(); // FirstPersonCameraComponent->GetComponentRotation();

	//**end of the trace will be straight out 10k units
	FVector EndLocation = StartLocation + Direction.Vector() * 10000;

	//**Parameters for the collision 
	FCollisionQueryParams QueryParams;
	//**Add the player character (aka this) to the collision ignore list
	QueryParams.AddIgnoredActor(this);
	QueryParams.bIgnoreBlocks = false;
	//**don't ignore touches
	QueryParams.bIgnoreTouches = false;

	//**Do line trace, and save if good or bad
	bool GoodTrace = GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_WorldDynamic, QueryParams);

	//**Get the rotation by reversing the Hit's normal
	FRotator SpawnRot = (HitResult.Normal * -1).Rotation();
	//**rotation offset for the portal object
	SpawnRot.Pitch += 90.0f;

	//**if there isn't an active portal, then spawn one
	if (!PortalActive)
	{
		//**if player has enough stamina
		if (Stamina >= 30.0f)
		{
			//**if trace was a successful hit
			if (GoodTrace)
			{	
				//**spawn portal at location of the trace hit
				Portal = GetWorld()->SpawnActor<APortal>(APortal::StaticClass(), HitResult.ImpactPoint, SpawnRot);

				//**activate portal
				Portal->SetActive();
				
				//**set active toggle
				PortalActive = true;

				//**Array to hold the static mesh(es) of the actor hit by trace, then intialized
				TArray<UStaticMeshComponent*> StaticMeshComponents = TArray<UStaticMeshComponent*>();
				//**Get the portals's static mesh components and put in our array
				Portal->GetComponents<UStaticMeshComponent>(StaticMeshComponents);

				//**Step through the array to set the materials on the mesh(es)
				for (int32 i = 0; i < StaticMeshComponents.Num(); i++)
				{
					//**pointer to current mesh
					UStaticMeshComponent *CurrentStaticMeshPtr = StaticMeshComponents[i];

					//**Set the material for the current mesh 
					CurrentStaticMeshPtr->SetMaterial(0, MaterialToApplyToClickedObject);

					//**Create a dynamic material instance for the current mesh
					UMaterialInstanceDynamic *MID = CurrentStaticMeshPtr->CreateAndSetMaterialInstanceDynamic(0);

					//**Cast the RenderTarget to a UTexture
					UTexture *CastedRenderTarget = Cast<UTexture>(RenderTarget);

					//**Set the texture for the dynamic material
					MID->SetTextureParameterValue("InputTexture", CastedRenderTarget);
				}
				/**************************************************************************************************
				**************This code block checks if the line trace hit an already inplace portal
				**************then changes its material to pixel shader demo material.
				**************Commenting out in order to instead spawn a portal at the location of the line trace.
				**************************************************************************************************
				//**Get the actor hit by the trace
				AActor *HitActor = HitResult.GetActor();

				//**If the hit actor isn't NULL
				if (HitActor != NULL)
				{
					//**Only if the other actor is a portal
					if (HitActor->IsA(APortal::StaticClass()))
					{
						//**Cast the hit actor to a portal object
						APortal *Portal = Cast<APortal>(HitActor);
						//**set to active
						Portal->SetActive();

						//**Array to hold the static mesh(es) of the actor hit by trace, then intialized
						TArray<UStaticMeshComponent*> StaticMeshComponents = TArray<UStaticMeshComponent*>();

						//**Get the actor's static mesh components and put in our array
						HitActor->GetComponents<UStaticMeshComponent>(StaticMeshComponents);

						//**Step through the array
						for (int32 i = 0; i < StaticMeshComponents.Num(); i++)
						{
							//**pointer to current mesh
							UStaticMeshComponent *CurrentStaticMeshPtr = StaticMeshComponents[i];

							//**Set the material for the current mesh
							CurrentStaticMeshPtr->SetMaterial(0, MaterialToApplyToClickedObject);

							//**Create a dynamic material instance for the current mesh
							UMaterialInstanceDynamic *MID = CurrentStaticMeshPtr->CreateAndSetMaterialInstanceDynamic(0);

							//**Cast the RenderTarget to a UTexture
							UTexture *CastedRenderTarget = Cast<UTexture>(RenderTarget);

							//**Set the texture for the dynamic material
							MID->SetTextureParameterValue("InputTexture", CastedRenderTarget);
						}
					}
				}
				*/
			}
			//******Spawn the Engery Beam*****//
			// spawn the projectile at the muzzle. Location and rotation are the same as where we started and direction of the line trace above
			GetWorld()->SpawnActor<AEnergyBeam>(AEnergyBeam::StaticClass(), StartLocation, Direction);

			//**Use stamina
			Stamina -= 30.0f;

			// try and play a firing animation if specified
			if (FireAnimation != NULL)
			{
				// Get the animation object for the arms mesh
				UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
				if (AnimInstance != NULL)
				{
					AnimInstance->Montage_Play(FireAnimation, 1.f);
				}
			}
		}

		//**else show UI message saying low on stamina
		else
		{
			GameMode->SetUIMessage("Not enough stamina");
		}
	}

	//**else there is a portal active
	else
	{
		//**if the trace was good
		if (GoodTrace)
		{
			FVector Coords = HitResult.Location +  (HitResult.Normal * 20); 

			//**Set the transport coords and rotation
			SetTransCoords(Coords, HitResult.Normal.Rotation());

			//**Spawn the Engery Beam
			AEnergyBeam *ExitBeam = GetWorld()->SpawnActor<AEnergyBeam>(AEnergyBeam::StaticClass(), StartLocation, Direction);
			ExitBeam->SetSpeed(4.0f);

			//**spawn an exit portal
			GetWorld()->SpawnActor<AExitPortal>(AExitPortal::StaticClass(), HitResult.ImpactPoint, SpawnRot);

			GameMode->SetUIMessage("Transport Coords Set");
		}
	}

	

	
}

//**Release tertiary fire
void APaintball_COYCharacter::OnTertiaryFireReleased()
{
	//GameMode->SetUIMessage("Energy Beam Fired!");
	

}

//**Change the health value
void APaintball_COYCharacter::AddHealth(float HPChange)
{
	//**make the change to health
	Health += HPChange;
	//**clamp the health
	Health = FMath::Clamp(Health, 0.0f, 100.0f);

	//**createa and display UI message
	FString msg = "Gained " + FString::SanitizeFloat(HPChange) + " health";
	GameMode->SetUIMessage(msg);

}

//**Function to set the transportation coordinates if a Portal is open
void APaintball_COYCharacter::SetTransCoords(FVector Coords, FRotator Rotation)
{
	//**if Portal isn't NULL, ie if there is a portal spawned
	if (Portal != NULL)
	{
		if (Portal->CheckActive())
		{
			//**Set the transport coords. This will be the last place a splat was spawned
			Portal->SetTransCoords(Coords, Rotation);
		}
	}
}


//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void APaintball_COYCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

//Saving functions
void APaintball_COYCharacter::SavePixelShaderOutput() {
	PixelShading->Save();
}
void APaintball_COYCharacter::SaveComputeShaderOutput() {
	ComputeShading->Save();
}
void APaintball_COYCharacter::ModifyComputeShaderBlend(float NewScalar) {
	//ComputeShaderBlendScalar = NewScalar;
}

void APaintball_COYCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}


void APaintball_COYCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void APaintball_COYCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

void APaintball_COYCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void APaintball_COYCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void APaintball_COYCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APaintball_COYCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool APaintball_COYCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &APaintball_COYCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &APaintball_COYCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &APaintball_COYCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}
