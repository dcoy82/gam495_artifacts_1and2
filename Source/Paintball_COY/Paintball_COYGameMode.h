// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/GameModeBase.h"
#include "Paintball_COYGameMode.generated.h"


//**enum class for the game's state
UENUM(BlueprintType)
enum class EGameState : uint8
{
	GamePlaying,
	GameWon,
	GameLost
};

UCLASS(minimalapi)
class APaintball_COYGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintball_COYGameMode();

	//**Override tick function 
	virtual void Tick(float DeltaTime) override;
	
	/*Not used for now. Uncomment when adding menu functionality
	/** Remove the current menu widget and create a new one from the specified class, if provided.
	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);
	*/

	//**Text to display on the UI
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UIMessage")
	FText UIMessage;

	//**game score
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UIMessage")
	float Score;

	//**Trigger for creating a UI message
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UIMessage")
	bool HasUIMessage;

	//**bool for winning
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UIMessage")
	bool HasWon;

	//**bool for losing
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UIMessage")
	bool HasLost;

	//**Funtion to get the current UI message
	UFUNCTION(BlueprintPure, Category = "UIMessage")
	float GetScore();

	void ChangeScore(float ScoreChange);
	
	//**Funtion to get the current UI message
	UFUNCTION(BlueprintPure, Category = "UIMessage")
	FText GetUIMessage();

	//**Function to set the current UI message
	UFUNCTION(BlueprintCallable, Category = "UIMessage")
	void SetUIMessage(FString Message);

	//**Play the UI message
	UFUNCTION(BlueprintPure, Category = "UIMessage")
	bool PlayUIMessage();

	//**set the game's state
	UFUNCTION(BlueprintCallable, Category = "UIMessage")
	void SetGameState(EGameState NewState);

	//**Return the current game state
	UFUNCTION(BlueprintPure, Category = "UIMessage")
	EGameState GetCurrentState() const;

	//**The game's current state
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
	EGameState CurrentState;

protected:
	/** Called when the game starts. */
	virtual void BeginPlay() override;

	/** The widget class we will use as our menu when the game starts. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
	TSubclassOf<UUserWidget> StartingWidgetClass;

	/** The widget instance that we are using as our menu. */
	//**Not in use. Uncomment when implementing menu
	//UPROPERTY()
	//UUserWidget* CurrentWidget;

private:
	
	//**Handle changes to game state
	void HandleNewGameState(EGameState NewState);

	//**reference to the player character
	class APaintball_COYCharacter *PlayerChar; 

};



