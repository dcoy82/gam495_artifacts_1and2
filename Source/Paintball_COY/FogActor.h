// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Character.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "FogActor.generated.h"

UCLASS()
class PAINTBALL_COY_API AFogActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFogActor();

	//**Overridden event after the component is initialized
	virtual void PostInitializeComponents() override;

	//**Function to set the size of the fog
	void SetSize(float size);

	//**Function to reveal a portion of the fog in a set radius
	void RevealSmoothCircle(const FVector2D &pos, float radius);
	   
private:
	//**update the texture
	void UpdateTextureRegions(UTexture2D *Texture, int32 MipIndex, uint32 NumRegions, FUpdateTextureRegion2D *Regions, uint32 SrcPitch, uint32 SrcBpp, uint8 *SrcData, bool bFreeData);

	//**fog texture size
	static const int m_textureSize = 512;
	
	//**Static mesh component for the fog
	UPROPERTY()
	UStaticMeshComponent *m_squarePlane;

	//**2d texture for the fog
	UPROPERTY()
	UTexture2D *m_dynamicTexture;

	//**material interface for fog
	UPROPERTY()
	UMaterialInterface *m_dynamicMaterial;

	//**dynamic instance for the fog
	UPROPERTY()
	UMaterialInstanceDynamic *m_dynamicMaterialInstance;

	//**array for pixel data, set at 512*512 (the area of the texture)
	uint8 m_pixelArray[m_textureSize * m_textureSize];

	//**update texture region
	FUpdateTextureRegion2D m_wholeTextureRegion;

	//**cover size
	float m_coverSize;

	ACharacter* myCharacter;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
