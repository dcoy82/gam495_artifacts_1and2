// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_COYGameMode.h"
#include "Paintball_COYHUD.h"
#include "Paintball_COYCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintball_COYGameMode::APaintball_COYGameMode()
	: Super()
{
	//**enable tick event
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintball_COYHUD::StaticClass();

	//**Set UI message trigger to false
	HasUIMessage = false;

	//**Initialze the score
	Score = 0.0f;
	
	HasWon = false;
	HasLost = false; 
}

void APaintball_COYGameMode::BeginPlay()
{
	Super::BeginPlay();

	//**get a reference to the player
	PlayerChar = Cast<APaintball_COYCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)); 

	//**Set the game mode to playing
	SetGameState(EGameState::GamePlaying);

	//ChangeMenuWidget(StartingWidgetClass);
}

void APaintball_COYGameMode::Tick(float DeltaTime)
{
	//**if the player reference exists 
	if (PlayerChar)
	{
		//**if player health reaches 0
		if (PlayerChar->Health <= 0.0f && !HasLost)
		{
			//**Set state to lost
			SetGameState(EGameState::GameLost);
		}

	}
	
}

float APaintball_COYGameMode::GetScore()
{
	return Score;
}

void APaintball_COYGameMode::ChangeScore(float ScoreChange)
{
	Score += ScoreChange;
}

//**Get the UI message
FText APaintball_COYGameMode::GetUIMessage()
{
	return UIMessage;
}

//**Set a UI Message
void APaintball_COYGameMode::SetUIMessage(FString Message)
{
	UIMessage = FText::FromString(Message);
	//**Toggle message indicator
	HasUIMessage = true;
}

//**Trigger the UI message to play
bool APaintball_COYGameMode::PlayUIMessage()
{
	if (HasUIMessage)
	{
		//**Toggle the HasUIMessage
		HasUIMessage = false;
		return true;
	}

	else
	{
		return false;
	}
}

//**Set the game state
void APaintball_COYGameMode::SetGameState(EGameState NewState)
{
	//**set current state to the new one
	CurrentState = NewState;
	//**handle the new state
	HandleNewGameState(CurrentState);
}

//**get game state
EGameState APaintball_COYGameMode::GetCurrentState() const
{
	return CurrentState;
}

//**Handle changes in game state
void APaintball_COYGameMode::HandleNewGameState(EGameState NewState)
{
	//**switch on the new state
	switch (NewState)
	{
		//**Game being played
		case EGameState::GamePlaying:
			{
				
				break;
			}
		//**Game won
		case EGameState::GameWon:
			{
				HasWon = true;
				SetUIMessage("You have won!");
				break;
			}
		//**Game lost
		case EGameState::GameLost:
			{
				HasLost = true;
				SetUIMessage("You have lost!");
				break;
			}

		default:
			break;
	}
}

/**Change menu widget
void APaintball_COYGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}
	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}
*/