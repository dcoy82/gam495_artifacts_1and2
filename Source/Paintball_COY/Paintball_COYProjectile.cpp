// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_COYProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Paintball_COYCharacter.h"
#include "Components/SphereComponent.h"

APaintball_COYProjectile::APaintball_COYProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &APaintball_COYProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	//**Set splat toggle
	CanSplat = true; 
}

void APaintball_COYProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//**If splat toggle true and the other actor isn't another projectile
	if (CanSplat && !OtherActor->IsA(APaintball_COYProjectile::StaticClass()))
	{
		//**Get the rotation by reversing the Hit's normal
		FRotator SpawnRot = (Hit.Normal * -1).Rotation();

		//**Spawn at the hit's impact point
		FVector SpawnLoc = Hit.ImpactPoint;
		
		//**Spawn the  splat actor
		GetWorld()->SpawnActor<ASplatActor>(ASplatActor::StaticClass(), SpawnLoc, SpawnRot);

		//**Toggle so can only create one splat
		CanSplat = false;

		//**Get the playey character
		//PlayerChar = Cast<APaintball_COYCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

		//**Set the transport coords to where the splat was spawned
		//PlayerChar->SetTransCoords(SpawnLoc);
	}

	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
	}

	//**destroy on hit so doesn't bounce around
	Destroy();
}