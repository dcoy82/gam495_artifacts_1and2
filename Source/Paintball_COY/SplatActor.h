// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Runtime/CoreUObject//Public/UObject/ConstructorHelpers.h"
#include "SplatActor.generated.h"

UCLASS()
class PAINTBALL_COY_API ASplatActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASplatActor();
		
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	//**procedural mesh component
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent *mesh;

	//**material interface
	UPROPERTY()
	UMaterialInterface *m_dynamicMaterial;

	//**dynamic material instance
	UPROPERTY()
	UMaterialInstanceDynamic *m_dynamicMaterialInstance;

	//**material opacity value
	float MatOpacity; 

	//**vector for the splat size
	FVector SplatSize;
		

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//**function to generate the cube
	virtual void GenerateSplatMesh();

	//**create the box mesh
	virtual void CreateSplatMesh(FVector BoxRadius, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals,
		TArray <FVector2D> & UVs, TArray <FProcMeshTangent> & Tangents, TArray <FColor> & Colors);

	//**over ride the post create actor function
	virtual void PostActorCreated() override;

	//** override the post load function
	virtual void PostLoad() override;

	//**Overridden event after the component is initialized
	virtual void PostInitializeComponents() override;

	
	
};
