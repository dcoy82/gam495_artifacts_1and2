// Fill out your copyright notice in the Description page of Project Settings.

#include "SplatActor.h"


// Sets default values
ASplatActor::ASplatActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//**intialize the mesh component
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedSplatMesh"));

	//**set the mesh as the root component
	RootComponent = mesh;

	//**Set the material for the mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> SplatMaterial(TEXT("/Game/Materials/M_SplatMat.M_SplatMat"));
	if (SplatMaterial.Succeeded())
	{
		
		m_dynamicMaterial = SplatMaterial.Object;
	}

	//**set lifespan
	InitialLifeSpan = 1.5f; 

	//**set material oppacity value
	MatOpacity = 1.0f;

	//**set the splat size
	SplatSize = FVector(0.0f, 40.0f, 40.0f);
	
}

void ASplatActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	//**if m_dynamicMaterial successfully initialize
	if (m_dynamicMaterial)
	{
		//**create dynamic material instance
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);

	}

	//**if successfully instanced
	if (m_dynamicMaterialInstance)
	{
		//**Set a random color
		m_dynamicMaterialInstance->SetVectorParameterValue(FName("Color"), FVector(FMath::RandRange(0.0f, 100.0f), FMath::RandRange(0.0f, 100.0f), FMath::RandRange(0.0f, 100.0f)));
		//**set the dynamic material to the plane mesh
		mesh->SetMaterial(0, m_dynamicMaterialInstance);
	}
}

// Called when the game starts or when spawned
void ASplatActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASplatActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//**DeltaTime/InitialLifeSpan will equal the percentage of the life span that has passed. 
	//Subtract that from the material opactiry to have the material fade evening over the life 
	MatOpacity -= DeltaTime / InitialLifeSpan; 
	
	//**Set the color parameter to the new value
	m_dynamicMaterialInstance->SetScalarParameterValue(FName(TEXT("Opacity")), MatOpacity);

}

//**override the post actor create
void ASplatActor::PostActorCreated()
{
	//**call the function in the super class
	Super::PostActorCreated();

	//**Generate the box
	GenerateSplatMesh();
}

//**overide the post load function
void ASplatActor::PostLoad()
{
	//**call the function in the super class
	Super::PostLoad();

	//**Generate the box
	GenerateSplatMesh();
}

//**generate the box mexh
void ASplatActor::GenerateSplatMesh()
{
	//**Array for the vertices of the cube
	TArray<FVector> Vertices;

	//**array for the normals
	TArray<FVector> Normals;

	//**arrray for the tangents
	TArray<FProcMeshTangent> Tangents;

	//**arrya for texture coords
	TArray<FVector2D> TextureCoordinates;

	//**Array for the triangles
	TArray<int32> Triangles;

	//**Array for vertices colors
	TArray<FColor> Colors;
	
	//**call the create mesh function and pass all the arrays just created. This creates all of the vertices, normals, tangents, and UVs for the mesh
	CreateSplatMesh(SplatSize, Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors);

	//**create the mesh section and disable collision. This actually creates and displays the mesh in the game world. 
	mesh->CreateMeshSection(0, Vertices, Triangles, Normals, TextureCoordinates, Colors, Tangents, false);
		
}

//**create the box mesh.
void ASplatActor::CreateSplatMesh(FVector SplatSize, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals,
	TArray <FVector2D> & UVs, TArray <FProcMeshTangent> & Tangents, TArray <FColor> & Colors)
{
	
	//**vector array for the splat's 8 vertices
	FVector SplatVerts[8];

	//**generate random coords for each vertex
	SplatVerts[0] = FVector(-0.2f, 0.0f, FMath::RandRange(1.0f, SplatSize.Z));										//**top
	SplatVerts[1] = FVector(-0.2f, FMath::RandRange(1.0f, SplatSize.Y), FMath::RandRange(1.0f, SplatSize.Z));		//**upper right
	SplatVerts[2] = FVector(-0.2f, FMath::RandRange(1.0f, SplatSize.Y), 0.0f);										//**right
	SplatVerts[3] = FVector(-0.2f, FMath::RandRange(1.0f, SplatSize.Y), FMath::RandRange(-SplatSize.Z, -1.0f));		//**lower right
	SplatVerts[4] = FVector(-0.2f, 0.0f, FMath::RandRange(-SplatSize.Z, -1.0f));									//**bottom
	SplatVerts[5] = FVector(-0.2f, FMath::RandRange(-SplatSize.Y, -1.0f), FMath::RandRange(-SplatSize.Z, -1.0f));	//**lower left
	SplatVerts[6] = FVector(-0.2f, FMath::RandRange(-SplatSize.Y, -1.0f), 0.0f);									//**left
	SplatVerts[7] = FVector(-0.2f, FMath::RandRange(-SplatSize.Y, -1.0f), FMath::RandRange(1.0f, SplatSize.Z));		//**upper left


	//**reset the triangle array
	Triangles.Reset();

	//**number of vertices
	const int32 NumVerts = 8;

	//**reset colors array
	Colors.Reset();

	//**add slots for each vertex
	Colors.AddUninitialized(NumVerts);

	//**Add colors to the vertices in blocks of 3
	for (int i = 0; i < NumVerts / 3; i++)
	{
		Colors[i * 3] = FColor(255, 0, 0);
		Colors[i * 3 + 1] = FColor(0, 255, 0);
		Colors[i * 3 + 2] = FColor(0, 0, 255);
	}

	//**reset and initialize to the number of vertices
	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);
	Normals.Reset();
	Normals.AddUninitialized(NumVerts);
	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);

	//**Assign the vertices
	Vertices[0] = SplatVerts[0];
	Vertices[1] = SplatVerts[1];
	Vertices[2] = SplatVerts[2];
	Vertices[3] = SplatVerts[3];
	Vertices[4] = SplatVerts[4];
	Vertices[5] = SplatVerts[5];
	Vertices[6] = SplatVerts[6];
	Vertices[7] = SplatVerts[7];

	//**assign the triangles
	Triangles.Add(0);
	Triangles.Add(7);
	Triangles.Add(6);
	
	Triangles.Add(0);
	Triangles.Add(6);
	Triangles.Add(1);
	
	Triangles.Add(1);
	Triangles.Add(6);
	Triangles.Add(3);
	
	Triangles.Add(6);
	Triangles.Add(5);
	Triangles.Add(3);
	
	Triangles.Add(3);
	Triangles.Add(5);
	Triangles.Add(4);
	
	Triangles.Add(1);
	Triangles.Add(3);
	Triangles.Add(2);

	//**Assign normals
	Normals[0] = Normals[1] = Normals[2] = Normals[3] = Normals[4] = Normals[5] = Normals[6] = Normals[7] = FVector(1.0f, 0.0f, 0.0f); //**forward

	//**assign tangents
	Tangents[0] = Tangents[1] = Tangents[2] = Tangents[3] = Tangents[4] = Tangents[5] = Tangents[6] = Tangents[7] = FProcMeshTangent(0.0f, 0.0f, 1.0f);
	
	

	//**set the UVs
	UVs.Reset();
	UVs.AddUninitialized(NumVerts);
	UVs[0] = UVs[4] = FVector2D(0.f, 0.f);
	UVs[1] = UVs[5] = FVector2D(0.f, 1.f);
	UVs[2] = UVs[6] = FVector2D(1.f, 1.f);
	UVs[3] = UVs[7] = FVector2D(1.f, 0.f);

}

