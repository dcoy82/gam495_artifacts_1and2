// Fill out your copyright notice in the Description page of Project Settings.

#include "EnergyBeam.h"
#include "Components/InputComponent.h"


// Sets default values
AEnergyBeam::AEnergyBeam()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(10.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("NoCollision");

	
	RootComponent = CollisionComp;


	//**Create the static mesh component
	UStaticMeshComponent* StaticMeshOne = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Box"));
	

	//**Attached the mesh to the box component
	StaticMeshOne->SetupAttachment(RootComponent);

	//**Find the 3d model asset for the box in the starter content folders
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShpereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	//**If the visual asset was successfully found, then set its parameters
	if (ShpereVisualAsset.Succeeded())
	{
		//**Set the BoxeMesh to the visual asset we just found
		StaticMeshOne->SetStaticMesh(ShpereVisualAsset.Object);

		//**Set the relative location of the boxes
		StaticMeshOne->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));

		//**Set the scale size of the sphere
		StaticMeshOne->SetWorldScale3D(FVector(0.5f, 0.5f, 0.5f));

	}
		

	//**Set the material for the mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> BeamMaterial(TEXT("/Game/Materials/M_EnergyBeam"));
	if (BeamMaterial.Succeeded())
	{
		StaticMeshOne->SetMaterial(0, BeamMaterial.Object);
	}

	
	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 1000.f;
	ProjectileMovement->MaxSpeed = 5000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->ProjectileGravityScale = 0;

	// Die after 1 second by default
	InitialLifeSpan = 2.0f;

}

// Called when the game starts or when spawned
void AEnergyBeam::BeginPlay()
{
	Super::BeginPlay();


	
}

// Called every frame
void AEnergyBeam::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnergyBeam::SetRotation(FRotator Rotation)
{
	SetActorRotation(Rotation);
}

void AEnergyBeam::SetSpeed(float Speed)
{
	ProjectileMovement->Velocity *= Speed;
	//ProjectileMovement->SetVelocityInLocalSpace(ProjectileMovement->Velocity * Speed); 
}
